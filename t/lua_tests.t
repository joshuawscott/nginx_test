use Test::Nginx::Socket 'no_plan';
add_block_preprocessor(sub {
  my $block = shift;

  if (!defined $block->config) {
    $block->set_value("config", <<'_END_');
location = /t {
  set_by_lua_block $the_param {
    local raw_args = ngx.req.get_uri_args()
    return raw_args.the_param
  }
  rewrite_by_lua_block {
    if ngx.var.the_param == "" then
      return ngx.exit(ngx.HTTP_NOT_FOUND)
    end
  }
  echo "The Param: $the_param";
}
_END_
  }
});

run_tests();
__DATA__
=== TEST 2: Lua Time
This is a demonstration of using lua in the configuration

--- request
GET /t?the_param=HOWDY
--- response_body
The Param: HOWDY
--- error_code: 200
--- no_error_log
[error]

=== TEST 3: Lua Bad Times
--- request
GET /t?wrong_param=HOWDY
--- error_code: 404

--- no_error_log
[error]
